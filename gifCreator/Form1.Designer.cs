﻿namespace gifCreator
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFilesDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveGifDialog = new System.Windows.Forms.SaveFileDialog();
            this.B_openGif = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.NUD_delai = new System.Windows.Forms.NumericUpDown();
            this.B_createGif = new System.Windows.Forms.Button();
            this.LB_images = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.LB_imagesOutput = new System.Windows.Forms.ListBox();
            this.openGifDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_delai)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFilesDialog
            // 
            this.openFilesDialog.Filter = "Images|*.bmp;*.jpg;*.jpeg;*.png;*.tif;*.tiff";
            this.openFilesDialog.Multiselect = true;
            // 
            // saveGifDialog
            // 
            this.saveGifDialog.Filter = "Fichier Gif|*.gif";
            // 
            // B_openGif
            // 
            this.B_openGif.Location = new System.Drawing.Point(6, 19);
            this.B_openGif.Name = "B_openGif";
            this.B_openGif.Size = new System.Drawing.Size(81, 23);
            this.B_openGif.TabIndex = 0;
            this.B_openGif.Text = "Ouvrir un GIF";
            this.B_openGif.UseVisualStyleBackColor = true;
            this.B_openGif.Click += new System.EventHandler(this.B_openGif_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.NUD_delai);
            this.groupBox1.Controls.Add(this.B_createGif);
            this.groupBox1.Controls.Add(this.LB_images);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(300, 200);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Creer un GIF";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Délai (ms)";
            // 
            // NUD_delai
            // 
            this.NUD_delai.Location = new System.Drawing.Point(76, 48);
            this.NUD_delai.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.NUD_delai.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.NUD_delai.Name = "NUD_delai";
            this.NUD_delai.Size = new System.Drawing.Size(73, 20);
            this.NUD_delai.TabIndex = 4;
            this.NUD_delai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NUD_delai.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // B_createGif
            // 
            this.B_createGif.Location = new System.Drawing.Point(6, 171);
            this.B_createGif.Name = "B_createGif";
            this.B_createGif.Size = new System.Drawing.Size(116, 23);
            this.B_createGif.TabIndex = 3;
            this.B_createGif.Text = "Creer le GIF";
            this.B_createGif.UseVisualStyleBackColor = true;
            this.B_createGif.Click += new System.EventHandler(this.B_createGif_Click);
            // 
            // LB_images
            // 
            this.LB_images.FormattingEnabled = true;
            this.LB_images.Location = new System.Drawing.Point(155, 19);
            this.LB_images.Name = "LB_images";
            this.LB_images.Size = new System.Drawing.Size(139, 173);
            this.LB_images.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(143, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Sélectionner des images";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.LB_imagesOutput);
            this.groupBox2.Controls.Add(this.B_openGif);
            this.groupBox2.Location = new System.Drawing.Point(327, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(300, 200);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Récuprer les images d\'un GIF";
            // 
            // LB_imagesOutput
            // 
            this.LB_imagesOutput.FormattingEnabled = true;
            this.LB_imagesOutput.Location = new System.Drawing.Point(163, 14);
            this.LB_imagesOutput.Name = "LB_imagesOutput";
            this.LB_imagesOutput.Size = new System.Drawing.Size(131, 173);
            this.LB_imagesOutput.TabIndex = 1;
            // 
            // openGifDialog
            // 
            this.openGifDialog.Filter = "Fichier Gif|*.gif";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 224);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_delai)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFilesDialog;
        private System.Windows.Forms.SaveFileDialog saveGifDialog;
        private System.Windows.Forms.Button B_openGif;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox LB_images;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.OpenFileDialog openGifDialog;
        private System.Windows.Forms.ListBox LB_imagesOutput;
        private System.Windows.Forms.Button B_createGif;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown NUD_delai;
    }
}

