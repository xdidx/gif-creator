﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using Gif.Components;
using System.IO;

namespace gifCreator
{
    public partial class Form1 : Form
    {

        List<string> imageFilePaths = new List<string>();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {            
            if (openFilesDialog.ShowDialog() == DialogResult.OK)
            {
                for (int i = 0; i < openFilesDialog.FileNames.Length; i++)
                {
                    imageFilePaths.Add(openFilesDialog.FileNames[i]);
                    LB_images.Items.Add(Path.GetFileName(openFilesDialog.FileNames[i]));
                }
            }
        }

        private void B_openGif_Click(object sender, EventArgs e)
        {
            if (openGifDialog.ShowDialog() == DialogResult.OK)
            {
                //extract Gif
                string outputPath = "";
                GifDecoder gifDecoder = new GifDecoder();

                try
                {
                    gifDecoder.Read(openGifDialog.FileName);
                }
                catch (Exception)
                {
                    MessageBox.Show("Fichier incompatible");
                }

                for (int i = 0, count = gifDecoder.GetFrameCount(); i < count; i++)
                {
                    Image frame = gifDecoder.GetFrame(i);  // frame i
                    frame.Save(outputPath + openGifDialog.FileName + i.ToString() + ".png", ImageFormat.Png);
                    LB_imagesOutput.Items.Add(openGifDialog.FileName + i.ToString() + ".png");
                }
            }
        }

        private void B_createGif_Click(object sender, EventArgs ea)
        {
            if (saveGifDialog.ShowDialog() == DialogResult.OK)
            {
                AnimatedGifEncoder e = new AnimatedGifEncoder();
                e.Start(saveGifDialog.FileName);
                e.SetRepeat(0);//-1:no repeat,0:always repeat
                for (int i = 0; i < imageFilePaths.Count; i++)
                {
                    e.AddFrame(Image.FromFile(imageFilePaths[i]), (int)(NUD_delai.Value/10));
                }
                e.Finish();
            }

        }
    }
}
